import argparse
import asyncio
import signal
from ipaddress import ip_network

# Buat parser untuk mengatur parameter input
parser = argparse.ArgumentParser(description='Scan network for Minecraft servers')

# Tambahkan parameter
parser.add_argument('-n', '--network', default='0.0.0.0/0', help='IP network to scan')
parser.add_argument('-p', '--port', default=25565, type=int, help='Port to scan')
parser.add_argument('-t', '--thread', default=16, type=int, help='Maximum number of workers')
parser.add_argument('-o', '--output-file', default='server_ips.txt', help='Output file')

# Parsing parameter input
args = parser.parse_args()

# Buat generator IP yang akan dipindai
ip_generator = ip_network(args.network).hosts()

# Buat semaphore
sem = asyncio.Semaphore(args.thread)

# Fungsi yang akan dipanggil saat sinyal diberikan ke program
def signal_handler():
    # Cetak pesan
    print("Program terhenti")
    # Tutup loop asyncio
    asyncio.get_event_loop().stop()

# Buat fungsi yang akan digunakan oleh thread
async def scan_ip(ip):
    server_address = (str(ip), args.port)
    try:
        # Cetak IP yang dicek
        print("Mengecek " + ip)
        # Gunakan asyncio.open_connection untuk mengelola koneksi ke server
        reader, writer = await asyncio.open_connection(*server_address)
        # Kirim permintaan untuk mengetahui jenis server
        writer.write(b"\xFE")
        data = await reader.read(1024)
        # Cek apakah server adalah server Minecraft
        if data[0] == b"\xFF":
            # Cetak IP server Minecraft
            print("Ditemukan server Minecraft di " + ip)
            # Gunakan with open untuk menulis ke file secara otomatis
            async with open(args.output_file, 'a') as f:
                f.write(f'{ip}\n')
    except:
        pass

# Pindai setiap IP dengan port yang ditentukan menggunakan thread secara asyncronous
async def main():
    tasks = []
    for ip in ip_generator:
        # Acquire semaphore
        await sem.acquire()
        # Create task
        tasks.append(asyncio.create_task(scan_ip(ip)))
        # Release semaphore
        sem.release()
    # Wait until all tasks are completed
    await asyncio.wait(tasks)

# Memanggin signal_handler untuk menangani sinyal
signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)

# Jalankan loop
asyncio.run(main())
